# mrcsmooth

Downsample a density.

## Installing and running

Install the latest release from the python package manager

```bash
pip3 install mrcsmooth
```

Then run it

```bash
mrcsmooth
```

## Author

Christian Blau
